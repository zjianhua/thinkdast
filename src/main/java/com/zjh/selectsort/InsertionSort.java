package com.zjh.selectsort;

import java.util.Arrays;

/**
 * Created by zjh on 2017/10/10.
 */
public class InsertionSort {
    public static void swapElement(int[] array, int i, int j) {
        int temp = array[i];
        array[i] = array[j];
        array[j] = temp;
    }

    public static void insertionSort(int[] array) {
        for (int i = 1; i < array.length; i++) {
            int j = i;
            while (j > 0 && array[j] < array[j - 1]) {
                swapElement(array, j, j - 1);
                System.out.println(Arrays.toString(array));
                j--;
            }
        }
    }

    public static void main(String[] args) {
        int[] array = {9, 4, 2, 5, 7, 3, 2, 0};
        System.out.println(Arrays.toString(array));
        insertionSort(array);
    }
}
