package com.zjh.selectsort;

import java.util.Arrays;

/**
 * Created by zjh on 2017/10/10.
 */
public class BubbleSort {

    public static void swapElement(int[] array, int i, int j) {
        int temp = array[i];
        array[i] = array[j];
        array[j] = temp;
    }

    public static void bubbleSort(int[] array) {
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array.length - 1; j++) {
                if (array[j] > array[j + 1]) {
                    swapElement(array, j, j + 1);
                    System.out.println(Arrays.toString(array));
                }
            }
        }
    }

    public static void main(String[] args) {
        int[] array = {1, 4, 2, 5, 7, 3, 2, 0};
        System.out.println(Arrays.toString(array));
        bubbleSort(array);
    }
}
